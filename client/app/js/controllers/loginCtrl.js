'use strict';
(function(app) {
    app.controller('LoginCtrl', LoginCtrl);

    function LoginCtrl($scope, $location) {
        $scope.userCredentials = {};

        $scope.login = function() {
            $location.path('/profile');
        };

        $scope.register = function() {
            $location.path('/create');
        };
    }
})(pmpApp);