pmpApp.config(function($routeProvider) {
    $routeProvider.
        when('/login', {templateUrl: 'app/partials/login.tpl.html', controller: 'LoginCtrl'}).
        when('/create', {templateUrl: 'app/partials/register.tpl.html', controller: 'RegisterCtrl'}).
        when('/profile', {templateUrl: 'app/partials/profile.tpl.html', controller: 'ProfileCtrl'}).
        otherwise({redirectTo: '/login'});
});