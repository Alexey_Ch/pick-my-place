'use strict';

var Place = require('../models').Place;
var Lease = require('../models').Lease;

module.exports = {

    list: function (req, res, next) {
        Place.find({ is_occupied: false}).then(function(list) {
            res.send(list);
        }, function(reason) {
            // rejection
        });
    },

    rent: function (req, res, next) {
        Place.findOneAndUpdate({ _id: req.params.id, is_occupied: false}, {is_occupied: true})
            .then(function(ob) {
                res.json({
                    "status": "success",
                    data: ob,
                    message : "place successfully rented"
                })
            })
            .catch(function(err) {
                console.log("ERR");
                console.log(err);
                next(err);
            });
    },
    cancelRent: function (req, res, next) {

    },

  /**
   * Set place for lease
   *
   * @param req
   * @param req.body
   * @param req.body.owner_id
   * @param res
   * @param next
   */
    lease: function (req, res, next) {
      Place
        .find({
            owner_id: req.body.owner_id
        })
        .then(function (place) {
            if (place.is_occupied === false) {
                throw new Error({message: 'Can\'n lease the place - it\'s already been leased'});
            }
            place.is_occupied = false;
            place.save();
        })
        .then(function () {
            res.send({status: 'success'})
        })
        .catch(function (e) {
            console.log(e);
            next(e);
        });

    },

  /**
   * Cancel lease. Expecting having only one lease per owner
   *
   * @param req
   * @param req.body
   * @param req.body.owner_id
   * @param res
   * @param next
   */
    cancelLease: function (req, res, next) {
        Lease
          .count({
              owner_id: req.body.owner_id,
              lease_date: new Date.now // todo: verify
          })
          .then(function (count) {
              if (count) {
                  throw new Error({message: 'Can\'t cancel the Lease - The place has already been rented'});
              }
              return Place.findOneAndUpdate({owner_id: req.body.owner_id}, {is_occupied: true});
          })
          .then(function () {
              res.send({status: 'success'})
          })
          .catch(function (e) {
              console.log(e);
              next(e);
          });
    }
};