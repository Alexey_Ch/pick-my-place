'use strict';

var _ = require('lodash');
var User = require('../models').User;
var passport = require('passport');

var publicKeys = _.chain(User.schema.paths)
    .pickBy(function(value) { return value.selected !== false; })
    .keys()
    .value();

module.exports = {

    auth: function (req, res, next) {
        console.log('User auth');
        res.end('User auth');
    },

    register: function (req, res, next) {
        var newUser = new User({
            email: req.body.email,
            password: req.body.password
        });

        newUser.save().then(function(user) {
            console.log('User ' + req.body.email + ' has registered');
            user = _.pick(user, publicKeys);
            res.json({data: user, status: 'success'});
        }, function(err) {
            console.error(err);
            next(err);
        });
    },
    
    login: function(req, res, next) {
        // TODO check where to redirect after login
        console.log('User post login');
        res.redirect('/user/profile');
    },

    findOne: function(req, res, next) {
        // 56e95447407d02080d82f1db
        var id = req.params.id;

        User.findOne({_id: id}).then(function(user) {
            console.log(user);
            res.send(user);
        });
    }
};
