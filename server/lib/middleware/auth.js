'use strict';

module.exports = function () {
  var nextUserId = 0;

    return function (req, res, next) {
      if (req.session.isNew) {
        req.session.userId = nextUserId;
        nextUserId++;
      }

      console.log('User: ', req.session.userId);
      next();
    }
};
