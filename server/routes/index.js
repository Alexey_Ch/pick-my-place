'use strict';

var place = require('./place'),
    user  = require('./user');

module.exports.setup = function(app) {
    app.get('/', function(req, res) {
        //SPA entry point.
        console.log('/login.html');
        res.render('index.html');
    });

    place.setup(app);
    user.setup(app);
};