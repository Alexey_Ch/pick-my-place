'use strict';

var ctrl = require('../controllers/placeController');

exports.setup = function (app) {
    app.get('/place/list', ctrl.list);

    app.post('/place/:id/rent', ctrl.rent);

    app.post('/place/:id/rent/cancel', ctrl.cancelRent);

    app.post('/place/lease', ctrl.lease);

    app.post('/place/lease/cancel', ctrl.cancelLease);
};


