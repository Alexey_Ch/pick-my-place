'use strict';

var passport = require('passport');
var ctrl = require('../controllers/userController');

module.exports.setup = function(app){

	app.post('/user/register', ctrl.register);
	app.post('/user/auth', ctrl.auth);
    
    app.post('/user/login', passport.authenticate('local', { failureRedirect: '/login' }), ctrl.login);

    //todo: add REST logout

    //app.get('/user/logout', function(req, res) {
    //    console.log('logout')
    //    req.logout();
    //    res.redirect('/');
    //});

    //todo: will be available via SPA.
    //app.get('/user/profile',
    //    require('connect-ensure-login').ensureLoggedIn(),
    //    function(req, res){
    //        console.log('access to profile');
    //        res.redirect('/profile.html');
    //    });
	app.get('/user/:id', ctrl.findOne);
};