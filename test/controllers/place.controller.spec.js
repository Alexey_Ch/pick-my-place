'use strict';

var should = require('should');
var sinon = require('sinon');
var mocks = require('node-mocks-http');
var express = require('express');
var Promise = require('bluebird');

var Place = require('../../server/models/index').Place;

var placeRoutes = require('../../server/routes/place');

function buildResponse() {
    return mocks.createResponse({eventEmitter: require('events').EventEmitter})
}


describe('CONTROLLER: placeController', function () {
    var app, req, res, stub, testData;

    before(function() {
        app = express();
        placeRoutes.setup(app);
    });

    beforeEach(function() {
        res = buildResponse();
    });

    afterEach(function () {
        req = null;
        res = null;
        testData = null;
        if (stub) {
            stub.restore();
            stub = null;
        }
    });

    describe('#list', function () {

        beforeEach(function () {
            testData = [{is_occupied: false}, {is_occupied: false}, {is_occupied: true}];

            stub = sinon.stub(Place, 'find').returns(new Promise(function (resolve) {
                resolve(testData);
            }));
        });

        it('Should return data received through `Place.find` method.', function (done) {
            req = mocks.createRequest({
                method: 'GET',
                url: '/place/list'
            });

            res.on('send', function() {
                should(stub.called).be.ok;
                should(res.statusCode).be.equal(200);
                should(res._getData()).be.equal(testData);
                done();
            });

            app.handle(req, res);
        });

        it('Should fail on `PUT` request.', function(done) {
            req = mocks.createRequest({
                method: 'PUT',
                url: '/place/list'
            });

            res.on('send', function() {
                // PUT method should not exist.
                // This part of the code should never execute.
                done(new Error('Response received.'));
            });

            app.handle(req, res, function() {
                done();
            });
        });
    });

    //describe('#register', function () {
    //
    //    it('Should create a new place if a model is valid ', function () {
    //
    //    });
    //
    //    it('Should show validation error if a model is invalid ', function () {
    //
    //    });
    //});
});
