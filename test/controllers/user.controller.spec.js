'use strict';

describe('CONTROLLER: userController', function () {

    describe('#auth', function () {

        it('Should raise an error if user is not found', function () {
        
        });

        it('Should raise an error if user is a password is not match', function () {
        
        });
    });

    describe('#register', function () {

        it('Should create a new user if a model is valid ', function () {
        
        });

        it('Should show validation error if a model is invalid ', function () {
        
        });
    });
});