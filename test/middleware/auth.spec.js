'use strict';

var should = require('should');
var sinon = require('sinon');
var mocks = require('node-mocks-http');
var express = require('express');

var middleware = require('../../server/lib/middleware/auth');

describe('MIDDLEWARE: Auth', function() {
    var app, req, res;

    beforeEach(function() {
        app = express();
        req = mocks.createRequest();
        res = mocks.createResponse();
    });

    afterEach(function() {
        app = null;
        req = null;
        res = null;
    });

    it('Should add parameter to req', function(done) {
        done();
    });
})